---
slug: add_gps_track_to_any_sports_activity
date: 2021-07-18T00:00:00
authors:
  - vanous
categories:
  - About
---

# Add GPS track to any recorded sports activity

One of the most missing and wanted features of Gadgetbridge for sports
enthusiasts with a Miband (or other Huami device) is to track a sports activity
which requests a connection to the original app, to provide GPS tracking. At
this point, Gadgetbridge does not have this possibility and as a work around,
the [3rd party Heart Rate sensor
sharing](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Huami-Heartrate-measurement#bluetooth-heart-rate-sensor-sharing-to-another-apps)
has been the best option. This works quite well, but has a downside of some
activities/data being tracked only in Gadgetbridge while some other data are
only in other apps. 
<!-- more -->

### Import and link a GPS track

With the release of version 0.58.2 we have added the ability to import a GPS
track in the form of a GPX file (a file with a GPS recording) and link it to a
recorded sports activity inside Gadgetbridge. While this still requires an
extra GPS recording app, it allows us to have at least a single place where
activities live.

At the same time, this now offers a new possibility to link a GPX file to a
sports activity that did not even allow to have a GPS recording in the first
place - like outdoor rowing while using the rowing machine recording or pool
swimming (if you have a waterproof phone). And, it opens up a door for other
devices to provide some sports activity recording info and then link the GPS
externally... for example the WaspOS just added a first raw version of a
[SportsApp](https://github.com/daniel-thompson/wasp-os/commit/b0bab534ff6308a9d87a61ea88e5fd72c252b75e)
which is essentially a stopwatch and a step counter. 

<figure><img src=../images/gpx_track_01.jpg width=300 alt="Gadgetbridge Sports
Activity screenshot"><figcaption align="center"><b>Gadgetbridge Sports
Activity detail</b></figcaption></figure>

### How does it work

In your band/watch, you start recording an activity and you also start
recording tracking in your sports tracking app like [Open
Tracks](https://f-droid.org/packages/de.dennisguse.opentracks/) or
[FitoTrack](https://f-droid.org/packages/de.tadris.fitness/). As an extra
bonus, you can also set your device to provide Heart Rate to the sports
tracking app, like [documented
here](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Huami-Heartrate-measurement#bluetooth-heart-rate-sensor-sharing-to-another-apps).
After you are done with your sports activity and stop the recording on both the
band and in the tracking app, you sync the Sports Activities with Gadgetbridge
and you take the recording from tracking app and share it with Gadgetbridge -
we newly provide a dedicated GPX Receiver, so you can share a GPX from any app
in Android. Gadgetbridge will receive the GPX track and will copy it to it's
[files](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Data-Export-Import-Merging-Processing#user-content-export-import-folder)
folder. Then, looking at the details of the recorded activity in the Sports
Activities section of Gadgetbridge, you can tap the "edit GPX" <img
src=../images/gps_edit.svg height=25 alt="edit GPX icon"> icon and select the
required GPX track.

<figure><img src=../images/gpx_track_04.jpg width=300 alt="Gadgetbridge GPX
Receiver"><img src=../images/gpx_track_01a.jpg width=300 alt="Edit GPX
track"><figcaption align="center"><b>Gadgetbridge GPX Receiver and Editing GPX
track</b></figcaption></figure>

### What does it provide

This gives you a chance to have a single place for all records of activities
you have done with your band and also have the associated GPS data with it.
Often, the activity recorded by the band will have some extra details like
averages of steps and heart rate, number of steps and for some activities like
swimming or rowing you can also see the number of stokes and so on. Besides
creating a picture of the track to better identify the activity, we do not do
any extra data extraction from the GPX, since similarly to the GPS recording,
this is a very niche domain and we let those who are already doing a great job
with the tracking and mapping apps to calculate and show you some stats of the
recorded GPS data.

To see the track in more detail, you can of course use the "Show GPS Track" to
export or view the recording in another app - like in [Open
Tracks](https://f-droid.org/packages/de.dennisguse.opentracks/),
[FitoTrack](https://f-droid.org/packages/de.tadris.fitness/),
[OsmAnd~](https://f-droid.org/en/packages/net.osmand.plus), [AAT Another
Activity Tracker](https://f-droid.org/packages/ch.bailu.aat/) or others.

Visit our [wiki](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/) to see
a full step by step [manual with
screenshots](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Integrating-with-Sports-Tracking-apps).

<figure><img src=../images/gpx_track_02.jpg width=300 alt="OsmAnd
screenshot"><img src=../images/gpx_track_03.jpg width=300 alt="OsmAnd
screenshot"><figcaption align = "center"><b>OsmAnd
screenshots</b></figcaption></figure>
