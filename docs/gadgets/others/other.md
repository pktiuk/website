---
title: Others & unbranded
---

# Others & unbranded

## Scannable

{{ 3621|pull }} added support for Gadgetbridge to react to the advertising of any device that is scannable. Gadgetbridge will not be able to connect to the device, but can detect its advertising.

When such a device is detected, the following intent is broadcasted:

* :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_SCANNED`

In order to add a device as "Scannable", just add it as a test device from the debug menu, or follow the [Pairing unsupported gadgets](../../../basics/pairing/#pairing-unsupported-gadgets) page.
In any case, select "Scannable Device" as the device type.
The device will then appear in your devices overview.
To start scanning for it, click it one.

If it is configured to auto-connect, the device will be automatically scanned for, henceforth.

It is also possible to configure the following settings:

* Debounce timeout - once scanned, GB will wait for this amount of time before starting to scan for the device again
* Minimum unseen time - after re-starting to scan for a device, GB will ignore scans if the lie within this amount of time from the last detection. In other words: Only react to this device if it has been away for a certain amount of time
* Minimal RSSI threshold - only register this device as scanned if it's RSSI surpasses this threshold. Cou can find out about a devices RSSI through apps like `nRF connect`

Using Scannable devices require the "Reconnect by BLE scan" setting in the "Discovery and Pairing options" to be enabled.

## VESC {{ device("other_vesc") }}

BLDC controller VESC.

## UM25 {{ device("other_um25") }}

USB voltage meter.

## Vibratissimo {{ device("vibratissimo_any") }}

A... massage gadget... as you can probably call by its name.

Support for this gadget was added with {{ "8ba7bc7353"|commit }}.

To be real, support was added as a joke ({{ 1235|issue(41670) }}) in 2016, and it is not to be taken seriously. It was added after [the lawsuit](https://arstechnica.com/wp-content/uploads/2016/09/vibratorsuit.pdf){: target="_blank" } about [another vibrator collecting information without user consent.](https://arstechnica.com/tech-policy/2016/12/maker-of-internet-of-things-connected-vibrator-will-settle-privacy-suit/){: target="_blank" }
