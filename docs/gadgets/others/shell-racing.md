---
title: Shell Racing
---

# Shell Racing

## Motorsport Supercars {{ device("shell_racing_rc_motorsport") }}

Remote-control (RC) cars from Shell, as can be [seen here](https://brand-base.com/project/motorsports-bluetooth-remote-control/){: target="_blank" }.

> Shell Oil Company in its gas station offers loyalty program under name Shell ClubSmart. Members collect points which can be later used to get rewards. In 2020 they have offered a collection of die-cast car models under name Shell Motorsport. All models of the collection are equipped with electrical motor. One of the models - Nissan Formula E Gen 2 Car - contains a battery that can be used to control a model remotely over Bluetooth. In some markets a battery with a remote controller is available separately.

Source: [this GitHub gist.](https://gist.github.com/scrool/e79d6a4cb50c26499746f4fe473b3768){: target="_blank" }

All features of the original app are supported in Gadgetbridge, with some extras.

* Normal and Turbo speed modes
* Toggle lights
* Blinking lights
* Pre-defined tricks (circles and u-turns)
* Battery level and history
