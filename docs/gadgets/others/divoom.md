---
title: Divoom
---

# Divoom

## Pixoo {{ device("divoom_pixoo") }}

The Pixoo is a 16x16 LED display.

At time of writing, Gadgetbridge support is still very early and is not yet able to control the display. The following settings are implemented:

- Set time
- Alarms
- Screen brightness
- Time format
- Send app notifications
- Clap hands to wakeup device
- Power saving
- Set device name
