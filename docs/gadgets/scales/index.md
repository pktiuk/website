---
title: Scales
icon: material/scale-bathroom
search:
  exclude: true
---

# Scales

This category contains scales.

{{ file_listing() }}
