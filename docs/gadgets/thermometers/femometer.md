---
title: Femometer
---

# Femometer

## Vinca 2 {{ device("femometer_vinca_2") }}

A thermometer to measure body temperature.

Support was added to this gadget with {{ 3369|pull }}.

Supported features by Gadgetbridge:

* Device info
* Battery
* Temperature (with charts)
* Set time
* Alarm
* Configuration options
    * Measurement mode (quick/normal/precise)
    * Volume
    * Temperature unit
