---
title: Withings
---

# Withings

## Steel HR {{ device("withings_steel_hr") }}

Supported features by Gadgetbridge:

* Pairing
* Set time
* Watch hands calibration
* Language settings
* Set workout activity types (more then 30)
* Sleep and REM sleep
* Steps tracking
* HR measurements
* Activity tracking (steps, hr, duration, distance)
* Alarms (3) with description and smart wake up

Note that the above list has been created from source code of the implementation.

TODO: if you have the watch, fill in more details.
