---
title: SMA
---

# SMA

Gadgetbridge support for SMA devices is highly work-in-progress currently.

## Q2 {{ device("sma_q2") }}

This device is supported with the custom firmware [SMA-Q2-OSS](https://github.com/Emeryth/sma-q2-oss){: target="_blank" } (see also [this project](https://hackaday.io/project/85463-color-open-source-smartwatch){: target="_blank" }). 

Supported features by Gadgetbridge:

* Notifications

## Q3 {{ device("sma_q3") }}

Although there is [work on a custom firmware](https://hackaday.io/project/175577-hackable-nrf52840-smart-watch){: target="_blank" }, there is no direct support for this device yet. Since its also running Espruino, maybe Bangle.js works enough.
