---
title: Sony
---

# Sony

## Wena 3 {{ device("sony_wena_3") }}

Support for this gadget was added with {{ 3311|pull }}. Manufacturing has stopped and official support will be phased out over the coming years.

Supported features by Gadgetbridge:

* Bonding/pairing works
* Serial/firmware version
* Music Player screen
* Weather
* Notification forwarding with app-specific pattern settings
* Detect "Photography Pro" to enable the Xperia-specific camera control mode
* Display settings (language, power on on wrist flick, font size, etc)
* Menu and home screen customization
* Alarms
* Calendar sync
* Do Not Disturb
* Auto Power Off
* Button binding (long press / double click)
* LED color and vibration setting (for all apps at once)
* Health data sync seemingly works?
* Volume buttons, camera buttons, media buttons — they seem to Just Work™ through the HID profile of the device.

Not supported:

* IC/iD/QuikPay support
* Alexa support
* Qrio support
* Mamorio support
* Riiiver support
* Seiko head linkage function
* Firmware update

Known issues:

* Sometimes when going out of range for a long time, need to connect the device from Android settings, otherwise Camera and Music controls don't work
* Sleep data is flaky, see {{ 3338|issue }}.
* Non-ASCII notifications sometimes don't work well, see {{ 3359|issue }}.
* Battery drain seems to be much higher than with the official app

## Liveview {{ device("sony_ericsson_liveview") }}

Support for this gadget was added with {{ 445|pull }}.

> LiveView comes with a belt clip and watch band and features a 128x128 OLED display, and Bluetooth radio. LiveView was introduced on 28 September 2010 and released by the end of the year.

Source: [:material-wikipedia: Sony Ericsson LiveView](https://en.wikipedia.org/wiki/Sony_Ericsson_LiveView){: target="_blank" }.

## SWR12 {{ device("sony_swr12") }}

Support for this gadget was added with {{ 2021|pull }}.
