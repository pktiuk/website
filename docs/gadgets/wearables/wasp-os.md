---
title: Wasp-os
---

# Wasp-os {{ device("wasp_os") }}

[Wasp-os](https://github.com/wasp-os/wasp-os){:target="_blank"} is an open-source firmware, based on MicroPython, for smart
watches that are based on the nRF52 family of microcontrollers.

Support was added in {{ 2135 | pull }}

Currently this includes the hacker friendly PineTime from Pine64 as well as the Colmi P8, which is a popular device with watch modders.

The Gadgetbridge integration for wasp-os is very similar to the Bangle.js: the watch provides REPL-over-BLE via the Nordic UART service.
