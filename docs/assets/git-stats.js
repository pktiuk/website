/*
    Copyright (C) 2023 ysfchn

    This file is part of Gadgetbridge.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
document$.subscribe(() => {
    /*
        Fetch the latest version of Gadgetbridge and put under the 
        git repository name in navigation bar. 
        
        Material for Mkdocs normally have this feature, but it only exists 
        for Github and Gitlab repositories, so we mimic the same feature in here
        by ourselves.

        As F-Droid API doesn't have CORS headers, we can't call F-Droid directly
        without a proxy, so we use Gitlab API to get the metadata file of
        Gadgetbridge's F-Droid data and get the "CurrentVersion" from there.

        Again, Codeberg also doesn't have CORS headers at the time of writing
        this, so we can't track star and fork count without a proxy.
    */
    const repo_stats = document.querySelector('[data-md-component="source"] .md-source__repository');
    const repo_url = "https://gitlab.com/api/v4/projects/fdroid%2Ffdroiddata/repository/files/metadata%2Fnodomain.freeyourgadget.gadgetbridge.yml/raw"

    function getCurrentReleaseFromMetadata(metadata) {
        for (const line of metadata.split("\n")) {
            if (line.startsWith("CurrentVersion: "))
                return line.slice("CurrentVersion: ".length);
        }
    }

    function loadInfo(data) {
        const facts = document.createElement("ul");
        facts.className = "md-source__facts";
        const stat = document.createElement("li");
        stat.className = "md-source__fact md-source__fact--other";
        stat.innerText = data["version"];
        facts.appendChild(stat);
        repo_stats.appendChild(facts);
    }

    function fetchInfo() {
        fetch(repo_url).then((r) => r.text()).then((content) => {
            const currentVersion = getCurrentReleaseFromMetadata(content);
            const data = {
                "version": currentVersion
            };
            __md_set("__git_repo", data, sessionStorage);
            loadInfo(data);
        });
    }

    if (!document.querySelector('[data-md-component="source"] .md-source__facts')) {
        const cached = __md_get("__git_repo", sessionStorage);
        if ((cached != null) && (cached["version"])) {
            loadInfo(cached);
        } else {
            fetchInfo();
        }
    }
});