---
title: Install firmware & watchface
---

# Install firmware & watchface

!!! warning "Warning"
    Flashing an firmware or watchface to your gadget **has the potential to brick your device!** This has never happened to any of the developers through flashing, but remember you're doing this **at your own risk.**

If your gadget's firmware flashing feature is supported by Gadgetbridge, you can flash firmware files to your gadget. Gadgets can support various different types of files, like firmware updates, watchfaces, applications, fonts, resource files, GPS data, images and so on. 

## Using the installer

<div class="gb-step" markdown>
![](../../assets/static/screenshots/firmware_open_file.jpg){: height="600" style="height: 600px;" }
<div markdown>

Download the file to your Android device that you want to flash to your gadget, then when you try to open the file, you should be able to see Gadgetbridge's "FW/App installer" in the "Open with" menu.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/firmware_install.jpg){: height="600" style="height: 600px;" }
<div markdown>

Now, click on "Install" to install file to your gadget and follow on-screen prompts if applicable. The screen might look different depending on which type of firmware that you are going to install.

Make sure to connect your gadget to Gadgetbridge first, otherwise you won't able to see this screen.

</div></div>

## Watchfaces

Watchfaces are uploaded the same way as is a firmware update.

### Xiaomi and Amazfit

Watchfaces can be found for example on [amazfitwatchfaces.com](https://amazfitwatchfaces.com/){: target="_blank" }.

In the Amazfit Bip / Bip Lite, one custom watchface can be used. Amazfit Band 5 / Mi Band 5 have slots for 3 custom watchfaces.

Custom watchface cannot be deleted from the gadget with Gadgetbridge, but can be overwritten by another one. Also, the stock watchfaces always remain and can be chosen from at any time.

### Pebble

You can find them on many places. The official source for watchfaces is the corresponding section in the [Rebble store.](https://apps.rebble.io/en_US/watchfaces?dev_settings=true){: target="_blank" } There you can browse through a large collection of watchfaces and, following the button on the end of each watchface page, you can download them via the "Download PBW" button.

If you don't see the button, make sure there is `?dev_settings=true` query parameter at the end of the page URL. Without this parameter, you won't be able to see a "Download PBW" button in the webpage.

There are also several free and open-source watchfaces you can find here at Github. The Gadgetbridge team e.g. recommends:

* [Mario Time](https://github.com/ClusterM/pebble-mario){: target="_blank" }
* [Mini Dungeon](https://github.com/Torivon/MiniDungeon){: target="_blank" }
