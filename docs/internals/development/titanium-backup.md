---
title: Restore from Titanium Backup
---

# Restore from Titanium Backup

If after a Restore from Titanium Backup Gadgetbridge does not start, this document describes how to revive it.

## Analysis

Checking with `adb logcat` you see one or more messages like,

```
Failed to ensure /data/user/0/nodomain.freeyourgadget.gadgetbridge/files: mkdir failed: EACCES (Permission denied)
Cannot open database '/data/user/0/nodomain.freeyourgadget.gadgetbridge/databases/Gadgetbridge': Directory /data/user/0/nodomain.freeyourgadget.gadgetbridge/databases doesn't exist
Cannot open database '/data/user/0/nodomain.freeyourgadget.gadgetbridge/databases/Gadgetbridge': Unknown reason
SharedPreferenc: type=1400 audit(0.0:420): avc: denied { open } for name="nodomain.freeyourgadget.gadgetbridge_preferences.xml"
... open failed: EACCES (Permission denied)
```

Checking on-device, the directories do exist and seem to have the correct permissions - or even fixing the permissions and SELinux context does not help:

```
# ls -l /data/user/0/nodomain.freeyourgadget.gadgetbridge/databases
-rw------- 1 u0_a90 u0_a90   6533120 1970-01-01 01:24 ActivityDatabase
-rw------- 1 u0_a90 u0_a90     45656 1970-01-01 01:24 ActivityDatabase-journal
-rw-rw---- 1 u0_a90 u0_a90 111177728 2022-06-07 01:53 Gadgetbridge
-rw------- 1 u0_a90 u0_a90    140048 2022-06-07 01:53 Gadgetbridge-journal

# cat /data/system/packages.list | grep "nodomain.freeyourgadget.gadgetbridge"  | cut -d' ' -f2
10123

# chown -R 10123 /data/user/0/nodomain.freeyourgadget.gadgetbridge /data/user_de/0/nodomain.freeyourgadget.gadgetbridge
# restorecon -F -R /data/user/0/nodomain.freeyourgadget.gadgetbridge     # SELinux context
# restorecon -F -R /data/user_de/0/nodomain.freeyourgadget.gadgetbridge
```

But opening the database with `sqlite` works:

```
# sqlite3 Gadgetbridge 
SQLite version 3.28.0 2021-07-13 15:30:48
Enter ".help" for usage hints.
sqlite> .tables
ACTIVITY_DESCRIPTION                 NO1_F1_ACTIVITY_SAMPLE             
..
```

## Recovery

1. Uninstall Gadgetbridge completely.
2. Re-install (either from F-Droid or via `adb install`), but **do not yet start the app!**.
3. Unpack and copy the missing non-empty data from your TitaniumBackup `*.tar.gz` file.
    * `.external.nodomain.freeyourgadget.gadgetbridge/files` goes to `/sdcard/Android/data/nodomain.freeyourgadget.gadgetbridge/files`
    * `/data/data/nodomain.freeyourgadget.gadgetbridge/*` goes to `/data/user/0/nodomain.freeyourgadget.gadgetbridge/*` (only replace not yet existing directories which in your backup are non-empty, i.e. leave the cache dirs on device untouched). After having copied these, remember to `chown -R u0_a123:u0_a123` each of the copied directories, using the `u0` name owning the `/data/user/0/nodomain.freeyourgadget.gadgetbridge` directory (i.e. not the `*_cache` one). This is only necessary for this location; ownership on the SD card is adjusted automatically
5. Now start Gadgetbridge (it should start fine again).

If you restored to a device your gadget was paired to before, you should be done - and the device should pair fine. If you had to restore to a new device (e.g. as the old one broke), you also need to …

## Fix the pairing

If you restored to a device which was previously paired to your gadget, Gadgetbridge should simply have connected to it again without any issues. As you're still reading here, this was probably not the case. So most likely you restored to a different device. Now Gadgetbridge thinks it already knows your gadget and doesn't offer it to you for pairing - while your gadget does not know your new device and hence does not connect to it. To fix that, we need to make Gadgetbridge forget the gadget - which will mean all related data will be lost as well. Which we want to keep, so:

1. Backup/Export your data, see [Data management](./data-management.md).
2. Now delete your gadget(s) from Gadgetbridge.
3. Follow the pairing procedure specific for your gadget.
4. Once your device is paired again successfully, restore the data from your above export.

Now everything should show up fine again. But should at this point watch apps or watch faces be missing from the list in Gadgetbridge, one step of above restore process might be needed to repeat:

1. Quit Gadgetbridge.
2. Check which files/directories from your Titanuim Backup `.tar.gz`s `.external.nodomain.freeyourgadget.gadgetbridge/files` are missing in `/sdcard/Android/data/nodomain.freeyourgadget.gadgetbridge/files` and copy them over again.
3. Start Gadgetbridge.

Now everything should be back. Note that some things might need a little time to sync to your gadget (e.g. with the Pebble, it can take several minutes until weather data show up in supported watch faces again), so don't despair (too early).
