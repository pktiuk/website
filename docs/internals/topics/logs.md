---
title: Obtaining logs
---

# Obtaining logs

This article is about application logging in Android which consists of dumps a log of system messages, including stack traces when the device throws an error and messages that have been written by the application. These can be retrieved via [logcat](https://developer.android.com/studio/command-line/logcat.html#startingLogcat){: target="_blank" }. Gadgetbridge also provides a way to save the application log into a file, which can then be read.

## Log contents

Please keep in mind Gadgetbridge log files may contain **lots of personal information**, including but not limited to health data, unique identifiers (such as a device MAC address), music preferences, etc. Additionally, consider that if you used your device with the official app in the past, the Bluetooth log might contain information about your account on the vendor's cloud, including ways to access it. Consider editing the file and removing this information before sending the file to a public issue report.

When sharing a log file, please try to make it as short as possible. If you already had logging enabled, disable and re-enable it before doing the things that you want to get logged. Disable logging when you're done with that. That way, we will not have to wade through huge log files looking for the actual relevant lines.

## Accessing the log file

If you enable Gadgetbridge's logging in the "Settings → Write Log Files", the log file will be stored as `gadgetbridge.log` in the [data folder](../development/data-management.md#data-folder-location). See the relevant link to learn how to access the log file.

## Sharing the log file directly

Gadgetbridge offers the possibility to share the log file directly from the app, reducing the need to use the file manager approach outlined below. Go to "Debug" in the menu and click the "Share log" button, then choose which app and/or contact to share the log with.
