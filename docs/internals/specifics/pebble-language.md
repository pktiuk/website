---
title: Pebble Language Packs
---

# Pebble Language Packs

Language packs (.pbl) on the pebble serve two purposes:

* Adding a translation for the Pebble OS
* Adding new characters (eg. Chinese characters)

Only one language pack can be installed at a time and it is possible to switch between English and the installed language's pack language. When switching to English while a language pack is installed, additional characters made available by the language pack will still be readable when notifications include these.

There are official language packs which can be downloaded here:

<https://lp.rebble.io/v1/languages>{: target="_blank" } (choose a version that is closest to your firmware, choosing a wrong version will simply result in untranslated text)

Also a bunch of unofficial language packs exist:

<https://github.com/MarSoft/pebble-firmware-utils/wiki/Language-Packs>{: target="_blank" }

For Korean and Japanese:

<http://wh.to/pebble/wiki.php>{: target="_blank" }
