---
title: Mi Band 6 Firmware Update
---

# Mi Band 6 Firmware Update

--8<-- "blocks.md:firmware_danger"

## Getting the firmware

Since we may not distribute the firmware, you have to find it elsewhere :( Mi Band 6 firmware updates were never part of Mi Fit, but are only made available via OTA updates.

### Choosing the right firmware version

Device Name                      | HW Revisions                          |
---------------------------------|---------------------------------------|
Mi Smart Band 6                  | V0.82.18.3, V0.82.129.3               |

## Installing the firmware

Copy the desired Mi Band 6 `.fw` and `.res` files to your Android device and open the `.fw` file from any file manager on that device. The Gadgetbridge firmware update activity should then be started and guide you through the installation process. Repeat with the `.res` file (you don't have to do this if the previous version you flashed had exactly the same `.res` version).

If you get the error "Element cannot be installed" or "The file format is not supported" try a different file manager. [Amaze](https://f-droid.org/en/packages/com.amaze.filemanager/){: target="_blank" } should work.

## Known firmware versions

### Mi Band 6

fw ver   | tested | res ver | fw-md5 | res-md5 | Notes 
---------|--------|---------|--------|---------|-------
0.1.0.49 | yes    | ?       |      ? |       ? | Came preinstalled
1.0.0.26 | no     | 28      |      ? |       ? |
1.0.0.34 | no     | 28      |      ? |       ? |
1.0.1.32 | yes    | 36      | `406775c3e9d2942354f5c5d902f1db42` | `5ac1bca43346857cc7e557d3f6f9a4a3` |
1.0.1.36 | yes    | 36      | `778352bca006f705971023421ebec651` | `5ac1bca43346857cc7e557d3f6f9a4a3` |
1.0.3.22 | yes    | 44      | `4f466a7e6f887d37ad6306b21fab6756` | `af7c521b9e7bd585f73a819f810590fa` |
1.0.4.38 | yes    | 52      | `e634d14ed3a945e447342cacd7facb24` | `92fb9a6400381695d9bc62c199bfaeb3` | Use Gadgetbridge 0.60.0 or newer.
1.0.5.24 | yes    | 56      | `8dca34fbefb393bb162958891f42cf78` | `de45a4c45e8e3590b6fb3fcb990bc27a` |
1.0.6.10 | yes    | 59      | `110dcd20af531f7bdbedb36eab73e03a` | `22d80a32ac8472e3768027a9ceb35e59` |
1.0.6.16 | yes    | 59      | `73f6853caf05ecfb5256a0961c860933` | `22d80a32ac8472e3768027a9ceb35e59` |
