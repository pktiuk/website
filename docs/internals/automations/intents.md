---
title: Intents
---

# Intents

This page documents some of the Intent APIs available in Gadgetbridge. With intents, you can use some of the functions of Gadgetbridge programmatically by calling intents from 3rd party apps and scripts.

To get started with intents, first you need to enable intent access under "Settings" → "Intent API" in the main menu. Enable the intent categories that you want to use.

![](../../assets/static/screenshots/settings_intents.jpg){: height="600" style="height: 600px;" }

## Bluetooth Connection API

This API allows controlling Bluetooth connection via Intents. It must be enabled explicitly in the Settings, by checking the "Bluetooth Intent API" setting.

=== ":material-flash: Trigger"
    * :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_CONNECT`
    * :material-plus-box: **Extras**:
        * `EXTRA_DEVICE_ADDRESS` (string, required)
            * Specifies MAC address of the gadget to connect.

    In order to trigger a device connection from other app:

    ```bash
    adb shell am broadcast \
        -a "nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_CONNECT" \
        -e "EXTRA_DEVICE_ADDRESS" "xx:xx:xx:xx:xx"
    ```

=== ":material-antenna: On connected"
    When a device is connected, the following Intent is broadcasted:

    * :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_CONNECTED`
    * :material-plus-box: **Extras**:
        * `EXTRA_DEVICE_ADDRESS` (string)
            * MAC address of the gadget that has connected.

=== ":material-radar: On scanned"

    See the page for [Scannable](../../../gadgets/others/other/#scannable).

## Device Settings Intent API

This API allows for device-specific settings to be set through Intents. It must be explicitly enabled per-device, in the device preferences screen. (from home screen, ":material-dots-vertical: → Set preferences → Allow 3rd party apps to change settings")

The actual preferences supported by each device may vary. Right now, the only way to know the correct / supported preference keys is by checking the code.

!!! warning "Warning"
    Given the low-level access of this feature, it has the potential for crashing Gadgetbridge, **or even the device.**

=== ":material-flash: Trigger"
    * :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.action.SET_DEVICE_SETTING`
    * :material-plus-box: **Extras**:
        * `device` (string, required)
            * Specifies MAC address of the gadget to manage.
        * `key` (string, required)
            * Name of the configuration key.
        * `value` (any)
            * Value that will be set for given configuration's name. Value `null` deletes the given configuration.

Here are some examples:

### Toggle AOD

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.action.SET_DEVICE_SETTING" \
    -e "device" "xx:xx:xx:xx:xx" \
    -e "key" "always_on_display_mode" \
    -e "value" "always"
```

### Set screen brightness

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.action.SET_DEVICE_SETTING" \
    -e "device" "xx:xx:xx:xx:xx" \
    -e "key" "screen_brightness" \
    --ei "value" "100"
```

## Synchronize Data API

Synchronizes data between the gadget and Gadgetbridge.

=== ":material-flash: Trigger"
    * :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.command.ACTIVITY_SYNC`
    * :material-plus-box: **Extras**:
        * `dataTypesHex` (string)
            * A combined bit flag value as a string prefixed with `0x`. See below for available flags.

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.ACTIVITY_SYNC" \
    nodomain.freeyourgadget.gadgetbridge
```

You can optionally specify the data types to be synchronized, as hex values, as defined in [RecordedDataTypes](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/model/RecordedDataTypes.java){: target="_blank" } in the source code.

```java
public class RecordedDataTypes {
    public static final int TYPE_ACTIVITY     = 0x00000001;
    public static final int TYPE_WORKOUTS     = 0x00000002;
    public static final int TYPE_GPS_TRACKS   = 0x00000004;
    public static final int TYPE_TEMPERATURE  = 0x00000008;
    public static final int TYPE_DEBUGLOGS    = 0x00000010;
    public static final int TYPE_SPO2         = 0x00000020;
    public static final int TYPE_STRESS       = 0x00000040;
    public static final int TYPE_HEART_RATE   = 0x00000080;
    public static final int TYPE_PAI          = 0x00000100;
    public static final int TYPE_SLEEP_RESPIRATORY_RATE = 0x00000200;
    // ... [truncated code]

    public static final int TYPE_SYNC = TYPE_ACTIVITY | TYPE_SPO2 | TYPE_STRESS |
            TYPE_HEART_RATE | TYPE_PAI | TYPE_SLEEP_RESPIRATORY_RATE;
}
```

For example, if you want to only synchronize workouts:

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.ACTIVITY_SYNC" \
    -e "dataTypesHex" "0x00000002" \ # (1)!
    nodomain.freeyourgadget.gadgetbridge
```

1. `0x00000002` means `TYPE_WORKOUTS` according to the above `RecordedDataTypes` class.

## Export Database API

=== ":material-flash: Trigger"
    * :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.command.TRIGGER_EXPORT`
    * :material-plus-box: **Extras**: _None._

=== ":material-antenna: On succeeded"
    When exporting the database has succeeded, the following Intent is broadcasted:

    * :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.action.DATABASE_EXPORT_SUCCESS`
    * :material-plus-box: **Extras**: _None._

=== ":material-antenna: On failed"
    When exporting the database has failed, the following Intent is broadcasted:

    * :material-flask-empty: **Action**: `nodomain.freeyourgadget.gadgetbridge.action.DATABASE_EXPORT_FAIL`
    * :material-plus-box: **Extras**: _None._

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.TRIGGER_EXPORT" \
    nodomain.freeyourgadget.gadgetbridge
```

## Debug Actions API

This API allows usage of some custom actions that are also manually accessible from the app's "Debug" menu. It must be **explicitly enabled in Settings**, by checking the "Allow Debug Commands" option.

<!-- TODO: Add tabs -->

### Send a custom notification

Send a standard notification to your gadget, with all fields available for customization. All Extra flags are optional; placeholder values will be used when a flag isn't specified.

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.DEBUG_SEND_NOTIFICATION" \
    -e "type" "GENERIC_SMS" \
    -e "phoneNumber" "0123456789" \
    -e "sender" "Wall Entity" \
    -e "subject" "I live in your walls" \
    -e "body" "I am living in your walls. You may be concerned about this..." \
    nodomain.freeyourgadget.gadgetbridge
```

### Trigger a fake incoming call

Make your gadget act like a phone call is incoming. All Extra flags are optional; placeholder values will be used when a flag isn't specified.

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.DEBUG_INCOMING_CALL" \
    -e "caller" "Your Nightmare" \
    nodomain.freeyourgadget.gadgetbridge
```

### Change gadget MAC address

This intent changes a gadget's MAC address in Gadgetbridge's database. It is useful when replacing a device with one of the same model/brand, or when the MAC address of the gadget changed because of a factory reset.

Pre-requisites:

* The old gadget is still shown in Gadgetbridge.
* The new gadget **does NOT** exist in Gadgetbridge.
* The gadgets are of the same brand and model.

This intent will:

* Migrate all preferences from the old to the new MAC address, since preferences are saved by MAC address.
* Update the MAC address of the device in the Gadgetbridge database.
* Quit Gadgetbridge to force a reload.

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.DEBUG_SET_DEVICE_ADDRESS" \
    -e "oldAddress" "00:00:00:00:00:00" \
    -e "newAddress" "FF:FF:FF:FF:FF:FF" \
    nodomain.freeyourgadget.gadgetbridge
```

Remember to update the auth key for the new device in preferences, if necessary.

## PebbleKit API

It's possible to send notifications directly to the gadget using Intents, through [PebbleKit](../../basics/topics/pebblekit.md) notifications. Despite having PebbleKit in the name, these work for any gadget supported by Gadgetbridge.

In the notification settings page, set "Pebble Messages" to "When screen is off" or "Always". Then, you can send a message by using Intents.

```bash
adb shell am broadcast \
    -a "com.getpebble.action.SEND_NOTIFICATION" \
    -e "messageType" "PEBBLE_ALERT" \
    -e "notificationData" '[{"title":"PebbleKitTest","body":"Sent from Gadgetbridge!"}]'
```

## See also

See [Gadget-specific intents](./intents-gadget.md) to learn about dedicated intents for specific gadgets.
