---
icon: material/home
hide:
  - navigation
  - toc
---

<style>
    .center {
        text-align: center;
    }

    h1 {
        margin: .8em 0 !important;
        font-weight: bold !important;
        color: var(--md-default-fg-color) !important;
    }

    .md-content__button {
        display: none;
    }

    h1 .headerlink {
        display: none !important;
    }

    .md-container main .md-grid {
        max-width: 1000px;
    }
</style>

<div class="center" markdown="1">
![](./assets/static/logo.svg){: .gb-logo height="120" style="height: 120px" }

# Gadgetbridge

Gadgetbridge is a free and open source Android application that allows you to pair and manage various gadgets such as smart watches, bands, headphones, and more without the need for the vendor application. So in short, you can use Gadgetbridge instead of relying on your gadget's own proprietary app.

<a href="{{ constants.fdroid }}" target="_blank" style="display: inline-block; vertical-align: middle; margin-right: 15px; margin-top: 20px;" markdown="span">
    ![](./assets/static/get-it-on-fdroid.png){: height="60" style="height: 60px; border-radius: 0px;" }
</a>

[:simple-codeberg: Source code]({{ constants.git }}){: .gb-button .gb-button-secondary target="_blank" } [:simple-liberapay: Donate]({{ constants.liberapay }}){: .gb-button .gb-button-secondary target="_blank" } [:simple-weblate: Translate]({{ constants.weblate }}){: .gb-button .gb-button-secondary target="_blank" }

[:material-arrow-right: Get started](./basics/index.md){: .gb-button style="margin-top: 15px;" }
</div>

---

## Nightly releases

Nightly releases are updated more frequently and may be less stable than standard releases, and they are distributed by our F-Droid repository unlike standard releases.

[:material-export-variant: Add Nightly F-Droid repo]( {{ constants.fdroid_repo }} ){: .gb-button .gb-button-secondary target="_blank" }
