<!-- --8<-- [start:firmware_danger] -->

!!! danger "Danger"
    Installing firmwares **has the potential to brick your gadget**! That said, this has never happened to any of the developers, but remember **you're doing this at your own risk**. See [Install a firmware or watchface](../features/installer.md) to know how to install a firmware.

<!-- --8<-- [end:firmware_danger] -->

<!-- --8<-- [start:non_released_gadget] -->

!!! example "Not yet released"
    Support for this gadget has been added to Gadgetbridge's codebase, but is not yet released. It is already available in the [nightly releases.](../../index.md#nightly-releases)

<!-- --8<-- [end:non_released_gadget] -->
