---
title: Music
---

# Music

Some gadgets have support for displaying and controlling music playback. This can vary in features, like:

* Play and stop
* Forward and backwards (fast forward/rewind)
* Volume control
* Show currently playing track info

<div class="gb-figure-container" markdown="span">
    <div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/demo/demo_mi_band_music.png){: height="300" style="height: 300px;" }
        On gadget
    </div><div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/demo/demo_notification_music.png){: height="230" style="height: 230px;" }
        On phone
    </div>
</div>

## Setting up

Gadgetbridge will automatically send the playback to the gadget, if it is supported by the gadget. So you don't need to enable anything for this feature.

### Preferred player

Gadgetbridge will read playback information from the currently playing media application, but you can change this if you have multiple media applications.

<div class="gb-step" markdown>
![](../../assets/static/screenshots/settings.jpg){: height="600" style="height: 600px;" }
<div markdown>

Under Gadgetbridge general settings, enable "Broadcast Media Button Intents Directly", then set a "Preferred Audioplayer" from one of the media applications that are installed on your phone.

</div></div>

## Troubleshooting

For advanced functions like track info or volume control, you have to make sure that you put your audio player into list of applications allowed to send notifications. It should be allowed by default if you didn't change anything.
