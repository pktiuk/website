---
title: Notifications
---

# Notifications

By default, Gadgetbridge will send notifications to the gadget when your Android device receives a notification unless your phone screen is on, but you can configure notification settings as you wish.

## Setting up

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sidebar_notifications.jpg){: height="600" style="height: 600px;" }
<div markdown>

To access the notification settings, tap the **:material-menu: Menu** icon or swipe from the left side to the right on the home screen to access the sidebar, then tap **:material-bell-outline: Notification settings**.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/notification_settings_1.jpg){: height="600" style="height: 600px;" }
<div markdown>

This is the screen where you can configure various notification settings.

</div></div>

### Per-app notification filtering

<div class="gb-step" markdown>
![](../../assets/static/screenshots/notification_per_app_settings.jpg){: height="600" style="height: 600px;" }
<div markdown>

Scroll to the bottom until you find the "Applications list" option to change settings per application.

For each app, you can (depending on your "Use the Applications list to..." setting) enable or disable notification sending to your gadget.

* Check "NOTIF" to enable/disable standard Android notifications from that application sending to your gadget,
* Check "PEBBLE" to enable/disable PebbleKit notifications from that application sending to your gadget.

You can also search for a specific app instead. To select and de-select all apps, tap **:material-dots-vertical: Menu** icon on the top-right.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/notification_per_app_word.jpg){: height="600" style="height: 600px;" }
<div markdown>

If you need even more fine-grained settings, you can filter notifications based on their content by tapping the **:material-cog: Settings** icon next to the application name. 

</div></div>

### Persistent notification

<div class="gb-step" markdown>
![](../../assets/static/screenshots/notification_persistent_1.png){: height="600" style="height: 600px;" }
<div markdown>

Gadgetbridge puts a persistent notification in the notifications drawer. This is required by recent Android versions for applications that need to keep running in the background. This is obviously preferred for Gadgetbridge, as it is the only way for Gadgetbridge to keep the Bluetooth connection with the gadgets active and to be able to send notifications and receive activity data.

However if you want to minimize or hide the connection status notification, long tap on the notification and open notification settings.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/notification_persistent_2.png){: height="600" style="height: 600px;" }
<div markdown>

Connection status notifications are sent from "Connection Status" category.

Here then you can silent and minimize it (to not show the icon in the status bar), or you can completely disable the notification. Notification options may depend on your Android version.

Either way, this won't negatively affect Gadgetbridge from running in the background.

</div></div>

### Device specific notification settings

Some devices allow you to receive additional notifications, here are few examples:

Notification when you reach your daily goal, like steps etc. Some devices might also offer specific vibrations settings or disconnected notifications, which can be set up also only for some Scheduled time interval period.

## Troubleshooting

### Notification access on Android 13+

Due to some changes to permissions granted to apps on Android 13+, it might not be possible to grant notification permissions to Gadgetbridge when installing it from F-Droid.

If you see a warning that notification access is a restricted setting, find Gadgetbridge in the Android settings, and click "Allow restricted settings" in the top right corner menu.

See {{ 3289|issue }} for more information.

### SMS notifications are lost somewhere

**Problem**

You are using alternative SMS app (like Silence). The stock SMS app is present and not disabled, but the alternative one is set up to be default. All notifications but SMS are shown on your gadget. The Gadgetbridge log says "Ignoring notification, is a system event".

**Solution**

You have to disable SMS support in Gadgetbridge settings to fix your problem (change it to "Never").

The reason is that we blacklist alternative SMS apps in the generic notification receiver since it would otherwise result in double notifications (one picked up from the Android SMS receiver, and one picked up from the actual notification).

In case the SMS is encrypted, it would not make sense to use the SMS receiver, but only the notification receiver which already contains the decrypted content.

### Incoming call notification on LineageOS

**Problem**

Using Gadgetbridge under LineageOS could prevent the notification of incoming calls on your gadget due to settings in the "privacy guard" of LineageOS.

**Solution**

Go to "Option → Security → Trust". Touch the more Options icon and then "Advanced". Search for Gadgetbridge and click on it. Set the reading of call log history to "Allowed".

See {{ 1477|issue }} for details.
