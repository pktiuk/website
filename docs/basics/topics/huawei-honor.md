---
title: Huawei/Honor gadgets
---

# Huawei/Honor gadgets

List of supported Huawei and Honor gadgets:

{% for key, device in device_support.items() | sort %}
    {%- if device.vendor == "huawei" or device.vendor == "honor" %}
        {% set name = key.replace("_", " ").title().replace(" Gt", " GT").replace(" Aw", " AW") %}
        {% set icon = ' :material-flask-empty-outline:{: title="Experimental" }' if "experimental" in device.flags else "" %}
* [{{ name }}](../../gadgets/wearables/huawei-honor.md#device__{{ key }}) {{ icon }}
    {% endif -%}
{% endfor %}


## Supported features

Support for Huawei and Honor devices was added in {{ 2462 | pull }}.

Note that not all devices have support for all features.

* Set the following band settings
    * Time
    * Language
    * Do not disturb
    * Inactivity warnings
    * Rotate wrist to switch info (only on some devices)
    * Activate display on wrist lift
    * Notification on bluetooth disconnect
* Alarms
    * Including smart alarm (only on some devices)
    * Retrieving alarms that were changed on the device[^1]
* Battery level synchronization[^1]
* Show notifications (has some bugs)
* Call notifications, with accept/reject (has some bugs on some devices, see [this for more information](../../internals/topics/huawei-honor-specifics.md#call-notifications))
* Music control
* Activity synchronization
    * Sleep synchronization (simple only, no TruSleep, none on Huawei Band 4 Pro)
    * Step count synchronization
    * Heart Rate synchronization (not supported by all devices)
    * SpO2 synchronization (not supported by all devices)
* Workout synchronization (which ones are really present depends on device and workout type)
    * HR for workout
    * speed
    * step rate
    * cadence
    * step length
    * ground contact time
    * impact
    * swing angle
    * fore foot landings
    * mid foot landings
    * back foot landings
    * eversion angle
    * strokes (swimming)
    * pool length (swimming)
    * swolf (swimming)
    * stroke rate (swimming)
* Find my phone, though it sometimes misbehaves
* Weather support (not supported by all devices)
    * Current weather
    * Hourly forecast
    * Daily forecast
    * Sunrise/set
    * Moonrise/set
    * Phase of the moon
* Sending GPS from the phone to the band during workouts[^2]

[^1]: Only retrieved/synchronized on activity or workout synchronization.
[^2]: We had hoped this would enable workouts like "Outdoor Cycling", but that is not working yet.

See the [Specifics for Huawei and Honor devices](../../internals/topics/huawei-honor-specifics.md) for an overview of how the Huawei/Honor devices differ from other devices in Gadgetbridge.

## Known issues

Some devices report the pairing to be failed, even though the pairing worked perfectly well.

## Not supported (yet)

* TruSleep
* Tidal information support
* Stress measuring
* Uploading watch faces
* Starting a workout from GB
* Real time data
* Firmware operations (like updating)
* Band info screen selection (choice & order)
* Sending phone volume to the band (for the music control)

