---
title: Background service
---

# Background service

![](../../assets/static/screenshots/demo/demo_background_service_notification.jpg)

You're on this page probably because you saw a notification with the content being something like, _"Failed to start background service"_.

This notification means that Gadgetbridge was prevented from starting the background service it needs in order to function properly. Make sure that your Android device doesn't prevent Gadgetbridge to start its background service.

## Possible fixes

* Enable "Autostart" under the application's settings available in the Settings **of your Android device.**
* Enable [CompanionDevice Pairing](../pairing/companion-device.md) in Gadgetbridge's settings. (available if you have Android 8 or up)
* Disable battery optimizations for Gadgetbridge.
* For Xiaomi devices running MIUI, allow "Start on boot" under "Other permissions".

If none of those work and you still see this notification, please open an issue on our [issue tracker.](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues)
