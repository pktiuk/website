---
title: List of requested permissions
---

# List of requested permissions

When Gadgetbridge requests a lot of permissions, this is due to the amount of supported features that require permissions, like notifications, call logs, phone state, calendar, and so on. One notable permission is access to location, which is caused by the Android permissions system: because discoverable devices might reveal information about the user's location, the device discovery process **requires location access.**

Since recent versions of Gadgetbridge, the location permission has also been used to provide GPS data to the gadget for [sports activities](../features/sports.md).

!!! success "Good to know"
    To ensure that none of the accessed data is ever forwarded anywhere, Gadgetbridge **does not have (and will not have) the Android `INTERNET` permission**, which means that Gadgetbridge has no means of sending any data anywhere. This is one of the most critical safeguards in the Gadgetbridge permission system.

!!! warning "Help needed"
    This page has been created in response to {{ 2062|issue }}. The permissions list and the initial explanation are taken from current manifest file of the Gadgetbridge source code. You can help us improve the page!

## Used for discovery and pairing

<div markdown="span">
**:material-bluetooth: Bluetooth**{ style="font-size: larger;" }
</div>

* android.permission.BLUETOOTH
    * Allows applications to connect to paired bluetooth devices.
* android.permission.BLUETOOTH_ADMIN
    * Allows applications to discover and pair bluetooth devices.

<div markdown="span" style="margin-top: 20px;">
**:material-map-marker: Location**{ style="font-size: larger;" }
</div>

* android.permission.ACCESS_COARSE_LOCATION
    * Because discoverable devices might reveal information about the user's location, the device discovery process requires location access.

## Used for background service

To keep the Bluetooth connection running even if the app is not currently in use at the moment, it needs a service to keep it from being killed by the system.

<div markdown="span">
**:material-cellphone-information: Other**{ style="font-size: larger;" }
</div>

* android.permission.RECEIVE_BOOT_COMPLETED
    * Allows Gadgetbridge to automatically start when the phone has finished booting up.
* android.permission.FOREGROUND_SERVICE
    * Used to start a foreground service (with accompanying persistent notification).
* android.permission.REQUEST_COMPANION_RUN_IN_BACKGROUND
    * Used to (re)start the background service while also being in the background, read more in [Companion device pairing](../pairing/companion-device.md).

## Used for automatic starting

Gadgetbridge services are started automatically on boot, notifications and Bluetooth, so you don't need to open Gadgetbridge for each time you restart your phone.

<div markdown="span">
**:material-cellphone-information: Other**{ style="font-size: larger;" }
</div>
* android.permission.RECEIVE_BOOT_COMPLETED
* android.permission.BIND_NOTIFICATION_LISTENER_SERVICE

<div markdown="span">
**:material-bluetooth: Bluetooth**{ style="font-size: larger;" }
</div>

* android.permission.BLUETOOTH
* android.permission.BLUETOOTH_ADMIN

## Used for "find phone"

<div markdown="span">
**:material-cellphone-information: Other**{ style="font-size: larger;" }
</div>
* android.permission.VIBRATE

## Used for weather provider access

<div markdown="span">
**:material-cellphone-information: Other**{ style="font-size: larger;" }
</div>
* cyanogenmod.permission.ACCESS_WEATHER_MANAGER
* cyanogenmod.permission.READ_WEATHER
* lineageos.permission.ACCESS_WEATHER_MANAGER
* lineageos.permission.READ_WEATHER
* org.omnirom.omnijaws.READ_WEATHER

## Used for workout GPS track recording

For workouts started from the band, if supported by the band and by Gadgetbridge.

<div markdown="span" style="margin-top: 20px;">
**:material-map-marker: Location**{ style="font-size: larger;" }
</div>

* android.permission.ACCESS_COARSE_LOCATION
* android.permission.ACCESS_FINE_LOCATION

## Required on Android 10 and up

This is to indicate access to user data, such a call log, calls, contacts and more.

<div markdown="span" style="margin-top: 20px;">
**:material-map-marker: Location**{ style="font-size: larger;" }
</div>

* android.permission.ACCESS_FINE_LOCATION
    * Because discoverable devices might reveal information about the user's location, the device discovery process requires location access.
* android.permission.ACCESS_BACKGROUND_LOCATION
    * "Only when the app is open" is unsuitable because Gadgetbridge runs the Bluetooth discovery scan in a background worker.
    * Because discoverable devices might reveal information about the user's location, the device discovery process requires location access.

<div markdown="span" style="margin-top: 20px;">
**:material-phone-log: Call logs**{ style="font-size: larger;" }
</div>

* android.permission.READ_CALL_LOG
* android.permission.PROCESS_OUTGOING_CALLS

<div markdown="span" style="margin-top: 20px;">
**:material-phone: Phone**{ style="font-size: larger;" }
</div>

* android.permission.READ_PHONE_STATE
* android.permission.CALL_PHONE
* android.permission.ANSWER_PHONE_CALLS
    * Call control, such as hanging up.

<div markdown="span" style="margin-top: 20px;">
**:material-message: SMS**{ style="font-size: larger;" }
</div>

* android.permission.RECEIVE_SMS
* android.permission.SEND_SMS

<div markdown="span" style="margin-top: 20px;">
**:material-contacts: Contacts**{ style="font-size: larger;" }
</div>

* android.permission.READ_CONTACTS

<div markdown="span" style="margin-top: 20px;">
**:material-calendar: Calendar**{ style="font-size: larger;" }
</div>

* android.permission.READ_CALENDAR
    * Used for syncing calendar events with gadget.

<div markdown="span" style="margin-top: 20px;">
**:material-folder: Storage**{ style="font-size: larger;" }
</div>

* android.permission.READ_EXTERNAL_STORAGE

<div markdown="span" style="margin-top: 20px;">
**:material-cellphone-information: Other**{ style="font-size: larger;" }
</div>

* android.permission.ACCESS_NOTIFICATION_POLICY
* android.permission.MEDIA_CONTENT_CONTROL
