---
title: Sports tracking apps
---

# Sports tracking apps

If you have a gadget that doesn't have GPS capabilities whatsoever, or if you want to have GPS recorded for special activities that your gadget doesn't record GPS by default, such as indoor cycling, rowing machine and pool swimming, you might consider using a dedicated 3rd party sports tracking app along with Gadgetbridge.

This allows you to share real-time heartrate data from your gadget to other applications, resulting in activities that include both GPS and heart rate data.

## Setting up

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_specific_settings_4.jpg){: height="600" style="height: 600px;" }
<div markdown>

Make sure to enable both "3rd party realtime HR access" and "Visible while connected" which can be found on "Device specific settings".

Once both of are enabled, Gadgetbridge no longer need to be running as the data will be always available via Bluetooth LE. For the heartrate data to be actually measured and sent, you need to either start a workout on your gadget, or have "Whole day HR measurement" feature enabled (feature availability may vary to the gadget). Because the whole day measurement detects activity automatically, once you get moving, heartrate measurements are taken frequently.

</div></div>

Now, open your 3rd party sports tracking app. This page will cover [OpenTracks](https://f-droid.org/packages/de.dennisguse.opentracks){: target="_blank" } and [FitoTrack](https://f-droid.org/packages/de.tadris.fitness){: target="_blank" } which not in a specific order. Note that you are not just limited to these two, any GPS tracking application that connects with Bluetooth wearables should also be fine.

## Connecting your gadget

Follow steps to connect your gadget with these applications.

### OpenTracks

<div class="gb-step" markdown>
![](../../assets/static/screenshots/opentracks_home.jpg){: height="600" style="height: 600px;" }
<div markdown>

Open **:material-dots-vertical: More** menu on top-right, then click "Settings" to go to the settings.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/opentracks_settings_heartrate.jpg){: height="600" style="height: 600px;" }
<div markdown>

From "Bluetooth sensors" category, find "Heart rate" and pick your gadget from the list.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/opentracks_settings_file_format.jpg){: height="600" style="height: 600px;" }
<div markdown>

Also, from "Import/Export" category, change "Export/sharing file format" to GPX, as you can only import GPX tracks back to Gadgetbridge.

Now start a workout on your gadget and in OpenTracks.

</div></div>

### FitoTrack

<div class="gb-step" markdown>
![](../../assets/static/screenshots/fitotrack_home.jpg){: height="600" style="height: 600px;" }
<div markdown>

Open **:material-plus-circle: Add** menu on bottom-right, then click "Record Workout".

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/fitotrack_select_device.jpg){: height="600" style="height: 600px;" }
<div markdown>

From **:material-dots-vertical: More** menu on top-right, click "Connect heart rate device" and pick your gadget from the list.

Now start a workout on your gadget and in FitoTrack.

</div></div>

## Importing to Gadgetbridge

To import a GPX track (which is usually recorded from 3rd party sports tracking apps as explained above) to Gadgetbridge, follow these steps:

<div class="gb-step" markdown>
![](../../assets/static/screenshots/gpx_receive_open.jpg){: height="600" style="height: 600px;" }
<div markdown>

From external application, share the GPX track, and from the available options choose "GPX Receiver".

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/gpx_receive.jpg){: height="600" style="height: 600px;" }
<div markdown>

Then click "OK" to finish.

If the given GPX file with the same name is already imported to Gadgetbridge, you will be asked for overwrite.

When done, [you can now link](../features/sports.md#linking-with-activities) an existing activity on Gadgetbridge with a GPX track.

</div></div>
